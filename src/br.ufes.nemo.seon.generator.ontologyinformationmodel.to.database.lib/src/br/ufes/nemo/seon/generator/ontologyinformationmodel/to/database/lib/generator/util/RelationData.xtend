package br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.generator.util

import org.eclipse.xtend.lib.annotations.Data

@Data
class RelationData {
	
	String relationsthipname
	String sourceClass
	String targetClass
	
	
}