package br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.generator

import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.generator.util.RelationData
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Attribute
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Configuration
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Entity
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.ManyToMany
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Module
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.OneToMany
import java.util.HashSet
import java.util.Set
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext

class EntityGenerator extends AbstractGenerator {
	
	
	var String lib_name;
	var PATH = "src/"
	var Set<Entity> superEntities;
	var Set<RelationData> relationsData;
	override doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
		
		superEntities = new HashSet()
		relationsData = new HashSet()
		
		for (configuration : resource.allContents.toIterable.filter(Configuration)) 
		{
			lib_name = configuration.lib.name.toLowerCase;
		}

		for (e: resource.allContents.toIterable.filter(Entity)) {
			if (e.superType != null){
				superEntities.add(e.superType);
			}
			
			for (r: e.relations){
				if (r.eClass.instanceClass == OneToMany){
					val RelationData relationData = new RelationData(r.name, e.name,r.type.name)
					relationsData.add(relationData)
				}
				
			}
		}

		// Criando os models, service e applications
		for (m : resource.allContents.toIterable.filter(Module)) {
			
			fsa.generateFile(PATH + lib_name + "/application/" + m.name.toLowerCase + "/__init__.py","")
			
			fsa.generateFile(PATH + lib_name + "/application/" + m.name.toLowerCase + "/application.py",m.createApplications)
			
			fsa.generateFile(PATH + lib_name + "/service/" + m.name.toLowerCase + "/__init__.py","")
			fsa.generateFile(PATH + lib_name + "/service/" + m.name.toLowerCase+ "/service.py",m.createServices)
			
			fsa.generateFile(PATH + lib_name + "/model/" + m.name.toLowerCase + "/__init__.py","")
			fsa.generateFile(PATH + lib_name + "/model/" + m.name.toLowerCase + "/models.py",m.createModels)
		}
		
		fsa.generateFile(PATH + lib_name + "/model/relationship/models.py",resource.createRelationships)
		fsa.generateFile(PATH + lib_name + "/model/relationship/__init__.py","")
		
		// Criando os métodos fabricas
		fsa.generateFile(PATH + lib_name + "/application/factories.py",resource.createApplicationFactory)
		fsa.generateFile(PATH + lib_name + "/service/factories.py",resource.createServiceFactory)
		fsa.generateFile(PATH + lib_name + "/model/factories.py",resource.createModelFactory)
		
		val modules = resource.allContents.toIterable.filter(Module);
		fsa.generateFile(PATH +"create_db.py", modules.create_db)
		
	}
	
	private def create_db(Iterable<Module> modules)'''
		from «lib_name».config.config import Config
		«FOR m: modules»
		from «lib_name».model.«m.name.toLowerCase».models import *
		«ENDFOR»
		
		conf = Config()
		conf.create_database()
	'''
	
	private def createRelationships(Resource resource)'''
	from «lib_name».config.config import Base
	from sqlalchemy import Column ,ForeignKey, Integer, Table
	from sqlalchemy.orm import relationship
	«FOR e: resource.allContents.toIterable.filter(Entity)»
		«FOR r: e.relations»
			«IF r.eClass.instanceClass == ManyToMany»
			« val ManyToMany manytomany = r as ManyToMany»
			« manytomany.createManytoMany(e)»
			«ENDIF»
		«ENDFOR»	
	«ENDFOR»
	'''
	
	private def createManytoMany(ManyToMany manyToMany, Entity e)'''
	
	association_«e.name.toLowerCase»_«manyToMany.type.name.toLowerCase»_table = Table('«e.name.toLowerCase»_«manyToMany.type.name.toLowerCase»', Base.metadata,
	    Column('«e.name.toLowerCase»_id', Integer, ForeignKey('«e.name».id')),
	    Column('«manyToMany.type.name.toLowerCase»_id', Integer, ForeignKey('«manyToMany.type.name.toLowerCase».id'))
	)
	
	'''
	
	private def createApplicationFactory(Resource resource)'''
	import factory
	«FOR m: resource.allContents.toIterable.filter(Module)»
	from «lib_name».application.«m.name.toLowerCase».application import *
	«ENDFOR»
	
	«FOR e: resource.allContents.toIterable.filter(Entity)»
	class «e.name»Factory(factory.Factory):
	    class Meta:
	        model = Application«e.name»
	
	«ENDFOR»
	'''
	
	private def createServiceFactory(Resource resource)'''
	import factory
	«FOR m: resource.allContents.toIterable.filter(Module)»
	from «lib_name».service.«m.name.toLowerCase».service import *
	«ENDFOR»
	
	«FOR e: resource.allContents.toIterable.filter(Entity)»
	class «e.name»Factory(factory.Factory):
	    class Meta:
	        model = «e.name»Service
	
	«ENDFOR»
	'''
	
	private def createModelFactory(Resource resource)'''
	import factory
	«FOR m: resource.allContents.toIterable.filter(Module)»
	from «lib_name».model.«m.name.toLowerCase».models import *
	«ENDFOR»
	
	«FOR e: resource.allContents.toIterable.filter(Entity)»
	class «e.name»Factory(factory.Factory):
	    class Meta:
	        model = «e.name»
	
	«ENDFOR»
	'''
	
	private def createServices(Module m)'''
	from sqlalchemy import create_engine
	from sqlalchemy.orm import sessionmaker
	from «lib_name».model.«m.name.toLowerCase».models import *
	from «lib_name».service.base_service import BaseService
	
	«FOR e: m.elements»
	«IF e.eClass.instanceClass == Entity»
		«val Entity entity = e as Entity»
		«entity.service»
	«ENDIF»
	«ENDFOR»
	'''
	private def service(Entity e)'''
	class «e.name.toFirstUpper»Service(BaseService):
		def __init__(self):
			super(«e.name.toFirstUpper»Service,self).__init__(«e.name.toFirstUpper»)
		
	'''
	
	private def createApplications(Module m)'''
	from «lib_name».service.«m.name.toLowerCase».service import *
	from «lib_name».application.abstract_application import AbstractApplication
	
	«FOR e: m.elements»
	«IF e.eClass.instanceClass == Entity»
		«val Entity entity = e as Entity»
		«entity.application»
	«ENDIF»
	«ENDFOR»
	'''
	private def application(Entity e)'''
	class Application«e.name.toFirstUpper»(AbstractApplication):
		def __init__(self):
		super().__init__(«e.name.toFirstUpper»Service())
		
	'''
	
	//Create a model
	// TODO --> Tratar relacioamentos de forma padrão
	// ManyToMany
	// ManyToOne
	//Fazer dos dois lados
	private def createModels(Module m)'''
	from «lib_name».config.base import Entity
	from sqlalchemy import Column, Boolean ,ForeignKey, Integer, DateTime, String, Text
	from sqlalchemy.orm import relationship
	from «lib_name».model.relationship.models import *
	«FOR e: m.elements»
	«IF e.eClass.instanceClass == Entity»
		«val Entity entity = e as Entity»
		«entity.model»
	«ENDIF»
	«ENDFOR»
	'''

	// Creating a model
	private def model(Entity e)'''
	
	class «e.name»«IF e.superType !== null»(«e.superType.name»)«ELSE»(Entity)«ENDIF»:
		«IF e.description != null»"""«e.description.textfield»"""«ENDIF»
		
		is_instance_of = "«e.entity_type.name»"
		__tablename__  = "«e.name.toLowerCase»"
	«IF superEntities.contains(e)»	type = Column(String(50))
	«ENDIF»
	«IF e.superType !== null»	id = Column(Integer, ForeignKey('«e.superType.name.toLowerCase».id'), primary_key=True)«ENDIF»
	«FOR a: e.attributes»
		«a.compile»
	«ENDFOR»
	«FOR r: e.relations»
		«IF r.eClass.instanceClass == OneToMany»	«r.name» = relationship("«r.type.name»", back_populates="«e.name.toLowerCase»") «ENDIF»
		«IF r.eClass.instanceClass == ManyToMany»	«r.name» = relationship("«r.type.name»", secondary=association_«e.name.toLowerCase»_«r.type.name.toLowerCase»_table) «ENDIF»
	«ENDFOR»
	«FOR rd: relationsData»
		«rd.compile(e)»
	«ENDFOR»
	«IF superEntities.contains(e)»	__mapper_args__ = {'polymorphic_identity':'team','polymorphic_on':type}«ENDIF»
	«IF e.superType !== null»	__mapper_args__ = {'polymorphic_identity':'«e.name.toLowerCase»',}«ENDIF»
		
	'''
	private def compile (Attribute a)'''
		«a.name» = Column(«a.type»)
		
	'''
	
	private def compile (RelationData rd, Entity e)'''
		«IF rd.targetClass.equals(e.name)»
			«rd.sourceClass.toLowerCase»_id = Column(Integer, ForeignKey('«rd.sourceClass.toLowerCase».id'))
			«rd.sourceClass.toLowerCase» = relationship("«rd.sourceClass»", back_populates="«rd.relationsthipname»") 
					
		«ENDIF»
	'''
		
	
}