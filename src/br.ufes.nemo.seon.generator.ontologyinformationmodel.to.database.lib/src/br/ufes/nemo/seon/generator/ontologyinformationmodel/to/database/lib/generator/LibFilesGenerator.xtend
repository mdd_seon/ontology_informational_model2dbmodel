package br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.generator

import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Configuration
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext

class LibFilesGenerator extends AbstractGenerator {
	
	var PATH = "src/"
	var lib_name = ""
	var GITLAB_PATH = "https://gitlab.com/mdd_seon/from_application_conceptual_data_model_2_lib_application"
	
	override doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
		
	for (configuration : resource.allContents.toIterable.filter(Configuration)) {
			lib_name = configuration.lib.name
			fsa.generateFile(PATH +"setup.py", configuration.setup)
	}

		fsa.generateFile(PATH +".gitignore", gitIgnore)

		fsa.generateFile(PATH +"pyproject.toml", pyproject)

		fsa.generateFile(PATH +"sonar-project.properties", sonarCompile)

		fsa.generateFile(PATH +"publish.sh", publish)
		
		fsa.generateFile(PATH +"requirements.txt", requirements)
	
	}
	
	def requirements()'''
		bleach==3.1.4
		certifi==2019.11.28
		chardet==3.0.4
		colorama==0.4.3
		commitizen==1.17.1
		decli==0.5.1
		docutils==0.16
		factory-boy==2.12.0
		Faker==4.0.2
		idna==2.9
		importlib-metadata==1.6.0
		Jinja2==2.11.1
		keyring==21.2.0
		MarkupSafe==1.1.1
		packaging==20.3
		pkginfo==1.5.0.1
		prompt-toolkit==3.0.5
		psycopg2==2.8.4
		Pygments==2.6.1
		pyparsing==2.4.6
		python-dateutil==2.8.1
		questionary==1.5.1
		readme-renderer==25.0
		requests==2.23.0
		requests-toolbelt==0.9.1
		six==1.14.0
		SQLAlchemy==1.3.15
		SQLAlchemy-serializer==1.3.4.2
		SQLAlchemy-Utils==0.36.3
		termcolor==1.1.0
		text-unidecode==1.3
		tomlkit==0.5.11
		tqdm==4.44.1
		twine==3.1.1
		urllib3==1.25.8
		wcwidth==0.1.9
		webencodings==0.5.1
		zipp==3.1.0
		
	
	'''
	
	def publish() '''
		rm -rf dist build
		python setup.py bdist_wheel
		python -m twine upload dist/*
	'''

	def setup(Configuration c) '''
	
		from setuptools import setup, find_packages
		
		with open("README.md", "r") as fh:
		    long_description = fh.read()
		
		setup(
		    name='«c.lib.name»',  # Required
		    version='0.0.1',  # Required
		    author="«c.author.name»",
		    author_email="«c.author_email.name»",
		    description="«c.about.name»",
		    long_description=long_description,
		    long_description_content_type="text/markdown",
		    url="«c.repository.name»",
		    packages=find_packages(),
		    
		    
		    install_requires=[
		        'SQLAlchemy', 'SQLAlchemy-Utils', 'psycopg2', 'factory-boy', 'SQLAlchemy-serializer'
		    ],
		    
		
		    classifiers=[
		         "Programming Language :: Python :: 3",
		         "License :: OSI Approved :: MIT License",
		         "Operating System :: OS Independent",
		     ],
		    setup_requires=['wheel'],
		    
		)
		
	'''

	def pyproject() '''
		[tool.commitizen]
		name = "cz_conventional_commits"
		version = "0.0.1"
		tag_format = "v$version"
		bump_message = "release $current_version → $new_version"
		version_files = [
		    "setup.py:version",
		    "pyproject.toml:version",
		]
		
		style = [
		    ["qmark", "fg:#ff9d00 bold"],
		    ["question", "bold"],
		    ["answer", "fg:#ff9d00 bold"],
		    ["pointer", "fg:#ff9d00 bold"],
		    ["highlighted", "fg:#ff9d00 bold"],
		    ["selected", "fg:#cc5454"],
		    ["separator", "fg:#cc5454"],
		    ["instruction", ""],
		    ["text", ""],
		    ["disabled", "fg:#858585 italic"]
		]
		
	'''

	def sonarCompile() '''
		sonar.projectKey = YOUR KEY
		sonar.projectName = «lib_name»
		sonar.projectVersion = 0.0.1
		sonar.sources = src
		sonar.language = py
		sonar.sourceEncoding = UTF-8
		sonar.host.url = http://sonarcloud.io
		sonar.login = YOUR SONAR LOGIN HASH
		
	'''

	def gitIgnore() '''
		# Byte-compiled / optimized / DLL files
		__pycache__/
		*.py[cod]
		*$py.class
		
		# C extensions
		*.so
		
		# Distribution / packaging
		.Python
		build/
		develop-eggs/
		dist/
		downloads/
		eggs/
		.eggs/
		lib/
		lib64/
		parts/
		sdist/
		var/
		wheels/
		share/python-wheels/
		*.egg-info/
		.installed.cfg
		*.egg
		MANIFEST
		
		# PyInstaller
		#  Usually these files are written by a python script from a template
		#  before PyInstaller builds the exe, so as to inject date/other infos into it.
		*.manifest
		*.spec
		
		# Installer logs
		pip-log.txt
		pip-delete-this-directory.txt
		
		# Unit test / coverage reports
		htmlcov/
		.tox/
		.nox/
		.coverage
		.coverage.*
		.cache
		nosetests.xml
		coverage.xml
		*.cover
		.hypothesis/
		.pytest_cache/
		
		# Translations
		*.mo
		*.pot
		
		# Django stuff:
		*.log
		local_settings.py
		db.sqlite3
		
		# Flask stuff:
		instance/
		.webassets-cache
		
		# Scrapy stuff:
		.scrapy
		
		# Sphinx documentation
		docs/_build/
		
		# PyBuilder
		target/
		
		# Jupyter Notebook
		.ipynb_checkpoints
		
		# IPython
		profile_default/
		ipython_config.py
		
		# pyenv
		.python-version
		
		# celery beat schedule file
		celerybeat-schedule
		
		# SageMath parsed files
		*.sage.py
		
		# Environments
		.env
		.venv
		env/
		venv/
		ENV/
		env.bak/
		venv.bak/
		
		# Spyder project settings
		.spyderproject
		.spyproject
		
		# Rope project settings
		.ropeproject
		
		# mkdocs documentation
		/site
		
		# mypy
		.mypy_cache/
		.dmypy.json
		dmypy.json
		
		# Pyre type checker
		.pyre/
		
	'''

		
	
}