package br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.generator

import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Configuration
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext

class HelpersGenerator extends AbstractGenerator {
	
	var String lib_name;
	var PATH = "src/"
	
	
	override doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
		
		for (configuration : resource.allContents.toIterable.filter(Configuration)) 
		{
			lib_name = configuration.lib.name.toLowerCase;
		}
		fsa.generateFile(PATH + lib_name + "/__init__.py","")
		
		fsa.generateFile(PATH + lib_name + "/application/__init__.py","")
		fsa.generateFile(PATH + lib_name + "/application/abstract_application.py",lib_name.createAbstractApplication)
		
		fsa.generateFile(PATH + lib_name + "/service/__init__.py","")
		fsa.generateFile(PATH + lib_name + "/service/base_service.py",lib_name.createBaseService)
		
		fsa.generateFile(PATH + lib_name + "/model/__init__.py","")
		fsa.generateFile(PATH + lib_name + "/model/core/__init__.py","")
		fsa.generateFile(PATH + lib_name + "/model/core/models.py",lib_name.createCoreModels)
		
		
		fsa.generateFile(PATH + lib_name + "/config/__init__.py","")
		fsa.generateFile(PATH + lib_name + "/config/base.py",createBase)
		fsa.generateFile(PATH + lib_name + "/config/config.py",createConfig)
		
		
	}
	
	def createCoreModels(String lib_name)'''
	from sqlalchemy import Column, String,ForeignKey, Integer
	from sqlalchemy.orm import relationship
	from sqlalchemy.ext.declarative import declarative_base
	from sqlalchemy_utils import UUIDType, URLType
	from «lib_name».config.base import Entity
	
	class ApplicationType (Entity):
	    
	    __tablename__ = "application_type"
	    application = relationship("Application", back_populates="application_type")
	    
	class Application(Entity):
	    
	    __tablename__ = "application"
	    application_type_id = Column(Integer, ForeignKey('application_type.id'))
	    application_type = relationship("ApplicationType", back_populates="application")
	    configuration = relationship("Configuration", back_populates="application")
	    
	    def __str__(self):
	        return self.name
	
	class Configuration(Entity):
	    
	    __tablename__ = "configuration"
	
	    secret = Column(String(200), nullable=False)
	    url = Column(URLType)
	    
	    application_id = Column(Integer, ForeignKey('application.id'))
	    application = relationship("Application", back_populates="configuration")
	    organization_id = Column(Integer, ForeignKey('organization.id'))
	    organization = relationship("Organization", back_populates="configuration") 
	
	
	class ApplicationReference(Entity):
	
	    __tablename__ = "application_reference"
	    # external application's data
	    configuration = Column(Integer, ForeignKey('configuration.id'))
	    
	    external_id = Column(String(200), nullable=False)
	    external_url = Column(URLType)
	    external_type_entity = Column(String(200), nullable=False)
	    
	    #Internal BD
	    internal_uuid = Column(UUIDType(binary=False))
	    entity_name = Column(String(200), nullable=False)
	
	
	'''
	
	def createBaseService(String lib_name)'''
	from sqlalchemy import create_engine
	from sqlalchemy.orm import sessionmaker
	from «lib_name».config.config import session
	from «lib_name».model.core.models import ApplicationReference
	
	class BaseService():
	    
	    def __init__(self, object):
	        self.session = session
	        self.object = object
	        self.type = self.object.__tablename__
	    
	    def get_all(self):
	        return self.session.query(self.object).order_by(self.object.id).all()
	    
	    def get_by_uuid(self, uuid):
	        return self.session.query(self.object).filter(self.object.uuid == uuid).first()
	        
	    def create(self, object):
	        self.session.add(object)
	        self.session.commit()
	
	    def update (self, object):
	        
	        self.session.query(self.object).filter(self.object.id == object.id).update({column: getattr(object, column) for column in self.object.__table__.columns.keys()})
	        self.session.commit()
	
	    def delete(self, object):
	        self.session.delete(object)
	        self.session.commit()
	
	'''
	
	def createAbstractApplication(String lib_name)'''
	from abc import ABC
	from sqlalchemy import update
	from pprint import pprint
	from «lib_name».model.core.models import ApplicationReference, Configuration
	
	class AbstractApplication(ABC):
	
	    def __init__(self, service):
	        self.service = service
	
	    def get_all (self):
	        return self.service.get_all()
	        
	    def create(self, object):
	        self.service.create (object)
	
	    def ___retrive_by_external_id_and_seon_entity_name (self, external_id, seon_entity_name):
	
	        if isinstance(external_id, int):
	            external_id = str(external_id)
	
	        return self.service.session.query(ApplicationReference).filter(ApplicationReference.external_id == external_id, 
	                                                               ApplicationReference.entity_name == seon_entity_name).first()
	
	    
	    def ___retrive_by_external_url_and_seon_entity_name (self, external_url, seon_entity_name):
	        return self.service.session.query(ApplicationReference).filter(ApplicationReference.external_url == external_url, 
	                                                               ApplicationReference.entity_name == seon_entity_name).first()
	
	    def retrive_by_external_url(self, external_url):
	
	        application_reference = self.___retrive_by_external_url_and_seon_entity_name(external_url, self.service.type)
	        if application_reference:
	            return self.service.get_by_uuid(application_reference.internal_uuid)
	        return None
	
	    def retrive_by_external_uuid(self, external_uuid):
	        
	        if isinstance(external_uuid, int):
	            external_uuid = str(external_uuid)
	
	        application_reference = self.___retrive_by_external_id_and_seon_entity_name(external_uuid, self.service.type)
	        if application_reference:
	            return self.service.get_by_uuid(application_reference.internal_uuid)
	        return None
	    
	    def ___retrive_by_external_id_and_seon_entity_name_and_configuration_uuid (self, external_id, seon_entity_name,configuration_uuid):
	
	        if isinstance(external_id, int):
	            external_id = str(external_id)
	        
	        if isinstance(configuration_uuid, int):
	            configuration_uuid = str(configuration_uuid)
	        
	        configuration = self.service.session.query(Configuration).filter(Configuration.uuid == configuration_uuid).first()
	
	        return self.service.session.query(ApplicationReference).filter(ApplicationReference.external_id == external_id, 
	                                                               ApplicationReference.entity_name == seon_entity_name,
	                                                               ApplicationReference.configuration == configuration.id).first()
	
	    def retrive_by_external_uuid_and_configuration_uuid(self, external_uuid, configuration_uuid):
	        
	        if isinstance(external_uuid, int):
	            external_uuid = str(external_uuid)
	        
	        if isinstance(configuration_uuid, int):
	            configuration_uuid = str(configuration_uuid)
	        
	        application_reference = self.___retrive_by_external_id_and_seon_entity_name_and_configuration_uuid(external_uuid, self.service.type,configuration_uuid)
	        if application_reference:
	            return self.service.get_by_uuid(application_reference.internal_uuid)
	        return None
	
	    def update (self, object):
	        self.service.update(object)
	    
	    def retrive_by_name (self, name):
	        return self.service.retrive_by_name(name)
	    
	    def get_by_uuid (self, uuid):
	        return self.service.get_by_uuid(uuid)
	    
	'''
	
	def createConfig()'''
		from sqlalchemy import create_engine
		from sqlalchemy.ext.declarative import declarative_base
		from sqlalchemy.orm import sessionmaker
		
		SQLALCHEMY_DATABASE_URI = "postgres://seon:seon@localhost/sspo_ontology"
		engine = create_engine(SQLALCHEMY_DATABASE_URI)
		session = sessionmaker(bind=engine)
		session = session()
		Base = declarative_base()
		
		class Config():
		
		    def create_database(self):
		        Base.metadata.create_all(engine)
	'''
	
	def createBase() '''
		from .config import Base
		import datetime
		from sqlalchemy import Column, String, Integer, DateTime, Table, ForeignKey, Text
		from sqlalchemy.orm import relationship
		from sqlalchemy_utils import UUIDType
		from sqlalchemy_serializer import SerializerMixin
		import uuid
		
		class Entity(Base, SerializerMixin):
		
		    __abstract__  = True
		    serialize_rules =('-id', "-uuid")
		    
		    id = Column(Integer, primary_key=True)
		    uuid = Column(UUIDType(binary=False), unique=True, nullable=False, default=uuid.uuid4)
		    date_created  = Column(DateTime,  default=datetime.datetime.utcnow)
		    date_modified = Column(DateTime,  default=datetime.datetime.utcnow,onupdate=datetime.datetime.utcnow)    
		
		    name = Column(String(200), nullable=True)
		    description = Column(Text(), nullable=True)
		    is_instance_of = ""
		
		    def entity_name(self):
		        return self.is_instance_of
		    
		    def __repr__(self):
		        return super().__repr__()
		
		    def to_dict(self):
		        dict = super().to_dict()
		        dict['uuid'] = str(self.uuid)
		        return dict

	'''
	
	
		
	
}