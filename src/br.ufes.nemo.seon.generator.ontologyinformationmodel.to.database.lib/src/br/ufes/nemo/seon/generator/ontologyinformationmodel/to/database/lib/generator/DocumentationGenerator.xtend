package br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.generator

import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Configuration
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Entity
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Function
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.ManyToMany
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Module
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.OneToMany
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.OneToOne
import com.google.inject.Inject
import java.util.ArrayList
import java.util.List
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.AbstractGenerator
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext
import org.eclipse.xtext.naming.IQualifiedNameProvider

class DocumentationGenerator extends AbstractGenerator {
	@Inject extension IQualifiedNameProvider
	var PATH = "src/"
	var GITLAB_PATH = "x"
	
	override doGenerate(Resource resource, IFileSystemAccess2 fsa, IGeneratorContext context) {
		var String lib_name = ""
		for (c : resource.allContents.toIterable.filter(Configuration)) {
				fsa.generateFile(PATH + "/README.md", c.compile)
				lib_name = c.lib.name
		}
		// criando o readme geral
		var Iterable<Module> modules = resource.allContents.toIterable.filter(Module);
		fsa.generateFile(PATH +"docs/README.md", modules.compile)
		
		modules = resource.allContents.toIterable.filter(Module);
		fsa.generateFile(PATH +"docs/packagediagram.puml", modules.createPackageDiagram)
		
		for (m : resource.allContents.toIterable.filter(Module)) {
				fsa.generateFile(PATH +"docs/"+m.name.toLowerCase+"/README.md", m.compile)
				fsa.generateFile(PATH +"docs/"+m.name.toLowerCase+"/classdiagram.puml", m.createClassDiagram)
			
		}
	}
	
	def createPackageDiagram(Iterable<Module> modules)'''
		@startuml
		«FOR m: modules»
			namespace «m.name»{
				
			}
		«ENDFOR»
		@enduml
	'''
	
	def compile (Iterable<Module> modules)'''
		# Documentation
		## Conceptual Model
		![Domain Diagram](packagediagram.png)	
								
		## Modules
		«FOR m: modules»
			«IF m.description !== null»
				* **[«m.name»](./«m.name»/)** : «m.description.textfield» 
				
				«ELSE»
				 * **[«m.name»](./«m.name»/)** : - 
				 
			«ENDIF»
		«ENDFOR»
		## Copyright
		This lib was PowerRight by [SEON Application Lib Generator](«GITLAB_PATH») 
	'''
	
	def createClassDiagram (Module m){
		val List<Entity> entities = new ArrayList()
		for (e: m.elements){
			if (e.eClass.instanceClass == Entity){
				val Entity entity = e as Entity
				entities.add(entity)
			}
		}
		'''
		«entities.createClassDiagram»
		'''
		
	}
	def createClassDiagram(List<Entity> entities) '''
		@startuml
		«IF entities.size()>0»
			«FOR e: entities»
				class «e.name»{
					«FOR a: e.attributes»
						«a.type»:«a.name»
					«ENDFOR»
					«FOR f: e.functions»
						«var count = 0»
						«f.type»: «f.name»(«FOR p: f.params»«p»«count = count+1»«IF count < f.params.size »,«ENDIF»«ENDFOR»)
					«ENDFOR»
				}
				«IF e.superType !== null»«e.superType.name» <|-- «e.name»«ENDIF»
				
				«FOR r: e.relations»
					«IF r.eClass.instanceClass == OneToOne»
						«e.name» "1" -- "1" «r.type.name» : «r.name.toLowerCase» >
					«ENDIF»
					«IF r.eClass.instanceClass == ManyToMany»
						«e.name» "0..*" -- "0..*" «r.type.name» : «r.name.toLowerCase» >
					«ENDIF»
					«IF r.eClass.instanceClass == OneToMany»
						«e.name» "1" -- "0..*" «r.type.name» : «r.name.toLowerCase» >
					«ENDIF»
				«ENDFOR»
				
			«ENDFOR»
		«ENDIF»
		
		@enduml
	'''

	
	def compile (Module m)
		'''
		# Documentation: «m.name»
		«m.description.textfield»
		
		## Application Conceptual Data Model
		![Domain Diagram](classdiagram.png)	
						
		## Entities
		«FOR e: m.elements»
			«IF e.eClass.instanceClass == Entity»
					«val Entity entity = e as Entity»
					«entity.descriptionX»
			«ENDIF»
		«ENDFOR»
		
		«FOR e: m.elements»
			«IF e.eClass.instanceClass == Entity»
				«val Entity entity = e as Entity»
				«IF entity.functions !== null && entity.functions.size() > 0»
					# Functions of «entity.name»:	 
					«FOR f: entity.functions»
						«f.descriptionX»
					«ENDFOR»		
				«ENDIF»
			«ENDIF»
		«ENDFOR»
		## Copyright
		This lib was PowerRight by [SEON Application Lib Generator](«GITLAB_PATH») 
		'''
	
		
	def descriptionX (Function f)'''
		«IF f.description !== null»
			* **«f.name»** : «f.description.textfield»«ELSE» * **«f.name»** : -
		«ENDIF»
	'''
	
	def descriptionX (Entity e)'''
		«IF e.description !== null»
			* **«e.name»** : «e.description.textfield»«ELSE»* **«e.name»** : -
		«ENDIF»
	
	'''
	
	
	def compile(Configuration e) '''
		# «e.lib.name.toFirstUpper»
		
		## General Information
		* **Software**:«e.software.name»
		* **Author**:«e.author.name»
		* **Author's e-mail**:«e.author_email.name»
		* **Source Repository**: [«e.repository.name»](«e.repository.name»)  
		
		## Goal
		«e.about.name»
		
		## Documentation
		
		The Documentation can be found in this [link](./docs/README.md)
			
		## Instalation
		
		To install «e.lib.name», run this command in your terminal:
		```bash
		pip install «e.lib.name»
		```
		
		## Usage
		
		```python
		#put your example of code here!
		```
		
		## Copyright
		This lib was PowerRight by [SEON Application Lib Generator](«GITLAB_PATH»)
		
			
	'''

	
	
	
}