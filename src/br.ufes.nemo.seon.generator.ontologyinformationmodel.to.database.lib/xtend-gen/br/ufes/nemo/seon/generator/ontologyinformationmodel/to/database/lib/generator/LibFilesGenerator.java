package br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.generator;

import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Configuration;
import com.google.common.collect.Iterables;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.AbstractGenerator;
import org.eclipse.xtext.generator.IFileSystemAccess2;
import org.eclipse.xtext.generator.IGeneratorContext;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;

@SuppressWarnings("all")
public class LibFilesGenerator extends AbstractGenerator {
  private String PATH = "src/";
  
  private String lib_name = "";
  
  private String GITLAB_PATH = "https://gitlab.com/mdd_seon/from_application_conceptual_data_model_2_lib_application";
  
  @Override
  public void doGenerate(final Resource resource, final IFileSystemAccess2 fsa, final IGeneratorContext context) {
    Iterable<Configuration> _filter = Iterables.<Configuration>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), Configuration.class);
    for (final Configuration configuration : _filter) {
      {
        this.lib_name = configuration.getLib().getName();
        fsa.generateFile((this.PATH + "setup.py"), this.setup(configuration));
      }
    }
    fsa.generateFile((this.PATH + ".gitignore"), this.gitIgnore());
    fsa.generateFile((this.PATH + "pyproject.toml"), this.pyproject());
    fsa.generateFile((this.PATH + "sonar-project.properties"), this.sonarCompile());
    fsa.generateFile((this.PATH + "publish.sh"), this.publish());
    fsa.generateFile((this.PATH + "requirements.txt"), this.requirements());
  }
  
  public CharSequence requirements() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("bleach==3.1.4");
    _builder.newLine();
    _builder.append("certifi==2019.11.28");
    _builder.newLine();
    _builder.append("chardet==3.0.4");
    _builder.newLine();
    _builder.append("colorama==0.4.3");
    _builder.newLine();
    _builder.append("commitizen==1.17.1");
    _builder.newLine();
    _builder.append("decli==0.5.1");
    _builder.newLine();
    _builder.append("docutils==0.16");
    _builder.newLine();
    _builder.append("factory-boy==2.12.0");
    _builder.newLine();
    _builder.append("Faker==4.0.2");
    _builder.newLine();
    _builder.append("idna==2.9");
    _builder.newLine();
    _builder.append("importlib-metadata==1.6.0");
    _builder.newLine();
    _builder.append("Jinja2==2.11.1");
    _builder.newLine();
    _builder.append("keyring==21.2.0");
    _builder.newLine();
    _builder.append("MarkupSafe==1.1.1");
    _builder.newLine();
    _builder.append("packaging==20.3");
    _builder.newLine();
    _builder.append("pkginfo==1.5.0.1");
    _builder.newLine();
    _builder.append("prompt-toolkit==3.0.5");
    _builder.newLine();
    _builder.append("psycopg2==2.8.4");
    _builder.newLine();
    _builder.append("Pygments==2.6.1");
    _builder.newLine();
    _builder.append("pyparsing==2.4.6");
    _builder.newLine();
    _builder.append("python-dateutil==2.8.1");
    _builder.newLine();
    _builder.append("questionary==1.5.1");
    _builder.newLine();
    _builder.append("readme-renderer==25.0");
    _builder.newLine();
    _builder.append("requests==2.23.0");
    _builder.newLine();
    _builder.append("requests-toolbelt==0.9.1");
    _builder.newLine();
    _builder.append("six==1.14.0");
    _builder.newLine();
    _builder.append("SQLAlchemy==1.3.15");
    _builder.newLine();
    _builder.append("SQLAlchemy-serializer==1.3.4.2");
    _builder.newLine();
    _builder.append("SQLAlchemy-Utils==0.36.3");
    _builder.newLine();
    _builder.append("termcolor==1.1.0");
    _builder.newLine();
    _builder.append("text-unidecode==1.3");
    _builder.newLine();
    _builder.append("tomlkit==0.5.11");
    _builder.newLine();
    _builder.append("tqdm==4.44.1");
    _builder.newLine();
    _builder.append("twine==3.1.1");
    _builder.newLine();
    _builder.append("urllib3==1.25.8");
    _builder.newLine();
    _builder.append("wcwidth==0.1.9");
    _builder.newLine();
    _builder.append("webencodings==0.5.1");
    _builder.newLine();
    _builder.append("zipp==3.1.0");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence publish() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("rm -rf dist build");
    _builder.newLine();
    _builder.append("python setup.py bdist_wheel");
    _builder.newLine();
    _builder.append("python -m twine upload dist/*");
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence setup(final Configuration c) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("from setuptools import setup, find_packages");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("with open(\"README.md\", \"r\") as fh:");
    _builder.newLine();
    _builder.append("\t    ");
    _builder.append("long_description = fh.read()");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("setup(");
    _builder.newLine();
    _builder.append("\t    ");
    _builder.append("name=\'");
    String _name = c.getLib().getName();
    _builder.append(_name, "\t    ");
    _builder.append("\',  # Required");
    _builder.newLineIfNotEmpty();
    _builder.append("\t    ");
    _builder.append("version=\'0.0.1\',  # Required");
    _builder.newLine();
    _builder.append("\t    ");
    _builder.append("author=\"");
    String _name_1 = c.getAuthor().getName();
    _builder.append(_name_1, "\t    ");
    _builder.append("\",");
    _builder.newLineIfNotEmpty();
    _builder.append("\t    ");
    _builder.append("author_email=\"");
    String _name_2 = c.getAuthor_email().getName();
    _builder.append(_name_2, "\t    ");
    _builder.append("\",");
    _builder.newLineIfNotEmpty();
    _builder.append("\t    ");
    _builder.append("description=\"");
    String _name_3 = c.getAbout().getName();
    _builder.append(_name_3, "\t    ");
    _builder.append("\",");
    _builder.newLineIfNotEmpty();
    _builder.append("\t    ");
    _builder.append("long_description=long_description,");
    _builder.newLine();
    _builder.append("\t    ");
    _builder.append("long_description_content_type=\"text/markdown\",");
    _builder.newLine();
    _builder.append("\t    ");
    _builder.append("url=\"");
    String _name_4 = c.getRepository().getName();
    _builder.append(_name_4, "\t    ");
    _builder.append("\",");
    _builder.newLineIfNotEmpty();
    _builder.append("\t    ");
    _builder.append("packages=find_packages(),");
    _builder.newLine();
    _builder.append("\t    ");
    _builder.newLine();
    _builder.append("\t    ");
    _builder.newLine();
    _builder.append("\t    ");
    _builder.append("install_requires=[");
    _builder.newLine();
    _builder.append("\t        ");
    _builder.append("\'SQLAlchemy\', \'SQLAlchemy-Utils\', \'psycopg2\', \'factory-boy\', \'SQLAlchemy-serializer\'");
    _builder.newLine();
    _builder.append("\t    ");
    _builder.append("],");
    _builder.newLine();
    _builder.append("\t    ");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t    ");
    _builder.append("classifiers=[");
    _builder.newLine();
    _builder.append("\t         ");
    _builder.append("\"Programming Language :: Python :: 3\",");
    _builder.newLine();
    _builder.append("\t         ");
    _builder.append("\"License :: OSI Approved :: MIT License\",");
    _builder.newLine();
    _builder.append("\t         ");
    _builder.append("\"Operating System :: OS Independent\",");
    _builder.newLine();
    _builder.append("\t     ");
    _builder.append("],");
    _builder.newLine();
    _builder.append("\t    ");
    _builder.append("setup_requires=[\'wheel\'],");
    _builder.newLine();
    _builder.append("\t    ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append(")");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence pyproject() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("[tool.commitizen]");
    _builder.newLine();
    _builder.append("name = \"cz_conventional_commits\"");
    _builder.newLine();
    _builder.append("version = \"0.0.1\"");
    _builder.newLine();
    _builder.append("tag_format = \"v$version\"");
    _builder.newLine();
    _builder.append("bump_message = \"release $current_version → $new_version\"");
    _builder.newLine();
    _builder.append("version_files = [");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("\"setup.py:version\",");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("\"pyproject.toml:version\",");
    _builder.newLine();
    _builder.append("]");
    _builder.newLine();
    _builder.newLine();
    _builder.append("style = [");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("[\"qmark\", \"fg:#ff9d00 bold\"],");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("[\"question\", \"bold\"],");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("[\"answer\", \"fg:#ff9d00 bold\"],");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("[\"pointer\", \"fg:#ff9d00 bold\"],");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("[\"highlighted\", \"fg:#ff9d00 bold\"],");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("[\"selected\", \"fg:#cc5454\"],");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("[\"separator\", \"fg:#cc5454\"],");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("[\"instruction\", \"\"],");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("[\"text\", \"\"],");
    _builder.newLine();
    _builder.append("    ");
    _builder.append("[\"disabled\", \"fg:#858585 italic\"]");
    _builder.newLine();
    _builder.append("]");
    _builder.newLine();
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence sonarCompile() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("sonar.projectKey = YOUR KEY");
    _builder.newLine();
    _builder.append("sonar.projectName = ");
    _builder.append(this.lib_name);
    _builder.newLineIfNotEmpty();
    _builder.append("sonar.projectVersion = 0.0.1");
    _builder.newLine();
    _builder.append("sonar.sources = src");
    _builder.newLine();
    _builder.append("sonar.language = py");
    _builder.newLine();
    _builder.append("sonar.sourceEncoding = UTF-8");
    _builder.newLine();
    _builder.append("sonar.host.url = http://sonarcloud.io");
    _builder.newLine();
    _builder.append("sonar.login = YOUR SONAR LOGIN HASH");
    _builder.newLine();
    _builder.newLine();
    return _builder;
  }
  
  public CharSequence gitIgnore() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("# Byte-compiled / optimized / DLL files");
    _builder.newLine();
    _builder.append("__pycache__/");
    _builder.newLine();
    _builder.append("*.py[cod]");
    _builder.newLine();
    _builder.append("*$py.class");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# C extensions");
    _builder.newLine();
    _builder.append("*.so");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# Distribution / packaging");
    _builder.newLine();
    _builder.append(".Python");
    _builder.newLine();
    _builder.append("build/");
    _builder.newLine();
    _builder.append("develop-eggs/");
    _builder.newLine();
    _builder.append("dist/");
    _builder.newLine();
    _builder.append("downloads/");
    _builder.newLine();
    _builder.append("eggs/");
    _builder.newLine();
    _builder.append(".eggs/");
    _builder.newLine();
    _builder.append("lib/");
    _builder.newLine();
    _builder.append("lib64/");
    _builder.newLine();
    _builder.append("parts/");
    _builder.newLine();
    _builder.append("sdist/");
    _builder.newLine();
    _builder.append("var/");
    _builder.newLine();
    _builder.append("wheels/");
    _builder.newLine();
    _builder.append("share/python-wheels/");
    _builder.newLine();
    _builder.append("*.egg-info/");
    _builder.newLine();
    _builder.append(".installed.cfg");
    _builder.newLine();
    _builder.append("*.egg");
    _builder.newLine();
    _builder.append("MANIFEST");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# PyInstaller");
    _builder.newLine();
    _builder.append("#  Usually these files are written by a python script from a template");
    _builder.newLine();
    _builder.append("#  before PyInstaller builds the exe, so as to inject date/other infos into it.");
    _builder.newLine();
    _builder.append("*.manifest");
    _builder.newLine();
    _builder.append("*.spec");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# Installer logs");
    _builder.newLine();
    _builder.append("pip-log.txt");
    _builder.newLine();
    _builder.append("pip-delete-this-directory.txt");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# Unit test / coverage reports");
    _builder.newLine();
    _builder.append("htmlcov/");
    _builder.newLine();
    _builder.append(".tox/");
    _builder.newLine();
    _builder.append(".nox/");
    _builder.newLine();
    _builder.append(".coverage");
    _builder.newLine();
    _builder.append(".coverage.*");
    _builder.newLine();
    _builder.append(".cache");
    _builder.newLine();
    _builder.append("nosetests.xml");
    _builder.newLine();
    _builder.append("coverage.xml");
    _builder.newLine();
    _builder.append("*.cover");
    _builder.newLine();
    _builder.append(".hypothesis/");
    _builder.newLine();
    _builder.append(".pytest_cache/");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# Translations");
    _builder.newLine();
    _builder.append("*.mo");
    _builder.newLine();
    _builder.append("*.pot");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# Django stuff:");
    _builder.newLine();
    _builder.append("*.log");
    _builder.newLine();
    _builder.append("local_settings.py");
    _builder.newLine();
    _builder.append("db.sqlite3");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# Flask stuff:");
    _builder.newLine();
    _builder.append("instance/");
    _builder.newLine();
    _builder.append(".webassets-cache");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# Scrapy stuff:");
    _builder.newLine();
    _builder.append(".scrapy");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# Sphinx documentation");
    _builder.newLine();
    _builder.append("docs/_build/");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# PyBuilder");
    _builder.newLine();
    _builder.append("target/");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# Jupyter Notebook");
    _builder.newLine();
    _builder.append(".ipynb_checkpoints");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# IPython");
    _builder.newLine();
    _builder.append("profile_default/");
    _builder.newLine();
    _builder.append("ipython_config.py");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# pyenv");
    _builder.newLine();
    _builder.append(".python-version");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# celery beat schedule file");
    _builder.newLine();
    _builder.append("celerybeat-schedule");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# SageMath parsed files");
    _builder.newLine();
    _builder.append("*.sage.py");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# Environments");
    _builder.newLine();
    _builder.append(".env");
    _builder.newLine();
    _builder.append(".venv");
    _builder.newLine();
    _builder.append("env/");
    _builder.newLine();
    _builder.append("venv/");
    _builder.newLine();
    _builder.append("ENV/");
    _builder.newLine();
    _builder.append("env.bak/");
    _builder.newLine();
    _builder.append("venv.bak/");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# Spyder project settings");
    _builder.newLine();
    _builder.append(".spyderproject");
    _builder.newLine();
    _builder.append(".spyproject");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# Rope project settings");
    _builder.newLine();
    _builder.append(".ropeproject");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# mkdocs documentation");
    _builder.newLine();
    _builder.append("/site");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# mypy");
    _builder.newLine();
    _builder.append(".mypy_cache/");
    _builder.newLine();
    _builder.append(".dmypy.json");
    _builder.newLine();
    _builder.append("dmypy.json");
    _builder.newLine();
    _builder.newLine();
    _builder.append("# Pyre type checker");
    _builder.newLine();
    _builder.append(".pyre/");
    _builder.newLine();
    _builder.newLine();
    return _builder;
  }
}
