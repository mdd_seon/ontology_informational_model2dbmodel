package br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.generator;

import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.generator.util.RelationData;
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.AbstractElement;
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Attribute;
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Configuration;
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Description;
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Entity;
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.ManyToMany;
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.OneToMany;
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Relation;
import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import java.util.HashSet;
import java.util.Set;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.generator.AbstractGenerator;
import org.eclipse.xtext.generator.IFileSystemAccess2;
import org.eclipse.xtext.generator.IGeneratorContext;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class EntityGenerator extends AbstractGenerator {
  private String lib_name;
  
  private String PATH = "src/";
  
  private Set<Entity> superEntities;
  
  private Set<RelationData> relationsData;
  
  @Override
  public void doGenerate(final Resource resource, final IFileSystemAccess2 fsa, final IGeneratorContext context) {
    HashSet<Entity> _hashSet = new HashSet<Entity>();
    this.superEntities = _hashSet;
    HashSet<RelationData> _hashSet_1 = new HashSet<RelationData>();
    this.relationsData = _hashSet_1;
    Iterable<Configuration> _filter = Iterables.<Configuration>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), Configuration.class);
    for (final Configuration configuration : _filter) {
      this.lib_name = configuration.getLib().getName().toLowerCase();
    }
    Iterable<Entity> _filter_1 = Iterables.<Entity>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), Entity.class);
    for (final Entity e : _filter_1) {
      {
        Entity _superType = e.getSuperType();
        boolean _notEquals = (!Objects.equal(_superType, null));
        if (_notEquals) {
          this.superEntities.add(e.getSuperType());
        }
        EList<Relation> _relations = e.getRelations();
        for (final Relation r : _relations) {
          Class<?> _instanceClass = r.eClass().getInstanceClass();
          boolean _equals = Objects.equal(_instanceClass, OneToMany.class);
          if (_equals) {
            String _name = r.getName();
            String _name_1 = e.getName();
            String _name_2 = r.getType().getName();
            final RelationData relationData = new RelationData(_name, _name_1, _name_2);
            this.relationsData.add(relationData);
          }
        }
      }
    }
    Iterable<br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Module> _filter_2 = Iterables.<br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Module>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Module.class);
    for (final br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Module m : _filter_2) {
      {
        String _lowerCase = m.getName().toLowerCase();
        String _plus = (((this.PATH + this.lib_name) + "/application/") + _lowerCase);
        String _plus_1 = (_plus + "/__init__.py");
        fsa.generateFile(_plus_1, "");
        String _lowerCase_1 = m.getName().toLowerCase();
        String _plus_2 = (((this.PATH + this.lib_name) + "/application/") + _lowerCase_1);
        String _plus_3 = (_plus_2 + "/application.py");
        fsa.generateFile(_plus_3, this.createApplications(m));
        String _lowerCase_2 = m.getName().toLowerCase();
        String _plus_4 = (((this.PATH + this.lib_name) + "/service/") + _lowerCase_2);
        String _plus_5 = (_plus_4 + "/__init__.py");
        fsa.generateFile(_plus_5, "");
        String _lowerCase_3 = m.getName().toLowerCase();
        String _plus_6 = (((this.PATH + this.lib_name) + "/service/") + _lowerCase_3);
        String _plus_7 = (_plus_6 + "/service.py");
        fsa.generateFile(_plus_7, this.createServices(m));
        String _lowerCase_4 = m.getName().toLowerCase();
        String _plus_8 = (((this.PATH + this.lib_name) + "/model/") + _lowerCase_4);
        String _plus_9 = (_plus_8 + "/__init__.py");
        fsa.generateFile(_plus_9, "");
        String _lowerCase_5 = m.getName().toLowerCase();
        String _plus_10 = (((this.PATH + this.lib_name) + "/model/") + _lowerCase_5);
        String _plus_11 = (_plus_10 + "/models.py");
        fsa.generateFile(_plus_11, this.createModels(m));
      }
    }
    fsa.generateFile(((this.PATH + this.lib_name) + "/model/relationship/models.py"), this.createRelationships(resource));
    fsa.generateFile(((this.PATH + this.lib_name) + "/model/relationship/__init__.py"), "");
    fsa.generateFile(((this.PATH + this.lib_name) + "/application/factories.py"), this.createApplicationFactory(resource));
    fsa.generateFile(((this.PATH + this.lib_name) + "/service/factories.py"), this.createServiceFactory(resource));
    fsa.generateFile(((this.PATH + this.lib_name) + "/model/factories.py"), this.createModelFactory(resource));
    final Iterable<br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Module> modules = Iterables.<br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Module>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Module.class);
    fsa.generateFile((this.PATH + "create_db.py"), this.create_db(modules));
  }
  
  private CharSequence create_db(final Iterable<br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Module> modules) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("from ");
    _builder.append(this.lib_name);
    _builder.append(".config.config import Config");
    _builder.newLineIfNotEmpty();
    {
      for(final br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Module m : modules) {
        _builder.append("from ");
        _builder.append(this.lib_name);
        _builder.append(".model.");
        String _lowerCase = m.getName().toLowerCase();
        _builder.append(_lowerCase);
        _builder.append(".models import *");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("conf = Config()");
    _builder.newLine();
    _builder.append("conf.create_database()");
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence createRelationships(final Resource resource) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("from ");
    _builder.append(this.lib_name);
    _builder.append(".config.config import Base");
    _builder.newLineIfNotEmpty();
    _builder.append("from sqlalchemy import Column ,ForeignKey, Integer, Table");
    _builder.newLine();
    _builder.append("from sqlalchemy.orm import relationship");
    _builder.newLine();
    {
      Iterable<Entity> _filter = Iterables.<Entity>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), Entity.class);
      for(final Entity e : _filter) {
        {
          EList<Relation> _relations = e.getRelations();
          for(final Relation r : _relations) {
            {
              Class<?> _instanceClass = r.eClass().getInstanceClass();
              boolean _equals = Objects.equal(_instanceClass, ManyToMany.class);
              if (_equals) {
                final ManyToMany manytomany = ((ManyToMany) r);
                _builder.newLineIfNotEmpty();
                CharSequence _createManytoMany = this.createManytoMany(manytomany, e);
                _builder.append(_createManytoMany);
                _builder.newLineIfNotEmpty();
              }
            }
          }
        }
      }
    }
    return _builder;
  }
  
  private CharSequence createManytoMany(final ManyToMany manyToMany, final Entity e) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    _builder.append("association_");
    String _lowerCase = e.getName().toLowerCase();
    _builder.append(_lowerCase);
    _builder.append("_");
    String _lowerCase_1 = manyToMany.getType().getName().toLowerCase();
    _builder.append(_lowerCase_1);
    _builder.append("_table = Table(\'");
    String _lowerCase_2 = e.getName().toLowerCase();
    _builder.append(_lowerCase_2);
    _builder.append("_");
    String _lowerCase_3 = manyToMany.getType().getName().toLowerCase();
    _builder.append(_lowerCase_3);
    _builder.append("\', Base.metadata,");
    _builder.newLineIfNotEmpty();
    _builder.append("    ");
    _builder.append("Column(\'");
    String _lowerCase_4 = e.getName().toLowerCase();
    _builder.append(_lowerCase_4, "    ");
    _builder.append("_id\', Integer, ForeignKey(\'");
    String _name = e.getName();
    _builder.append(_name, "    ");
    _builder.append(".id\')),");
    _builder.newLineIfNotEmpty();
    _builder.append("    ");
    _builder.append("Column(\'");
    String _lowerCase_5 = manyToMany.getType().getName().toLowerCase();
    _builder.append(_lowerCase_5, "    ");
    _builder.append("_id\', Integer, ForeignKey(\'");
    String _lowerCase_6 = manyToMany.getType().getName().toLowerCase();
    _builder.append(_lowerCase_6, "    ");
    _builder.append(".id\'))");
    _builder.newLineIfNotEmpty();
    _builder.append(")");
    _builder.newLine();
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence createApplicationFactory(final Resource resource) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("import factory");
    _builder.newLine();
    {
      Iterable<br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Module> _filter = Iterables.<br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Module>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Module.class);
      for(final br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Module m : _filter) {
        _builder.append("from ");
        _builder.append(this.lib_name);
        _builder.append(".application.");
        String _lowerCase = m.getName().toLowerCase();
        _builder.append(_lowerCase);
        _builder.append(".application import *");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    {
      Iterable<Entity> _filter_1 = Iterables.<Entity>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), Entity.class);
      for(final Entity e : _filter_1) {
        _builder.append("class ");
        String _name = e.getName();
        _builder.append(_name);
        _builder.append("Factory(factory.Factory):");
        _builder.newLineIfNotEmpty();
        _builder.append("    ");
        _builder.append("class Meta:");
        _builder.newLine();
        _builder.append("        ");
        _builder.append("model = Application");
        String _name_1 = e.getName();
        _builder.append(_name_1, "        ");
        _builder.newLineIfNotEmpty();
        _builder.newLine();
      }
    }
    return _builder;
  }
  
  private CharSequence createServiceFactory(final Resource resource) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("import factory");
    _builder.newLine();
    {
      Iterable<br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Module> _filter = Iterables.<br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Module>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Module.class);
      for(final br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Module m : _filter) {
        _builder.append("from ");
        _builder.append(this.lib_name);
        _builder.append(".service.");
        String _lowerCase = m.getName().toLowerCase();
        _builder.append(_lowerCase);
        _builder.append(".service import *");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    {
      Iterable<Entity> _filter_1 = Iterables.<Entity>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), Entity.class);
      for(final Entity e : _filter_1) {
        _builder.append("class ");
        String _name = e.getName();
        _builder.append(_name);
        _builder.append("Factory(factory.Factory):");
        _builder.newLineIfNotEmpty();
        _builder.append("    ");
        _builder.append("class Meta:");
        _builder.newLine();
        _builder.append("        ");
        _builder.append("model = ");
        String _name_1 = e.getName();
        _builder.append(_name_1, "        ");
        _builder.append("Service");
        _builder.newLineIfNotEmpty();
        _builder.newLine();
      }
    }
    return _builder;
  }
  
  private CharSequence createModelFactory(final Resource resource) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("import factory");
    _builder.newLine();
    {
      Iterable<br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Module> _filter = Iterables.<br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Module>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Module.class);
      for(final br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Module m : _filter) {
        _builder.append("from ");
        _builder.append(this.lib_name);
        _builder.append(".model.");
        String _lowerCase = m.getName().toLowerCase();
        _builder.append(_lowerCase);
        _builder.append(".models import *");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    {
      Iterable<Entity> _filter_1 = Iterables.<Entity>filter(IteratorExtensions.<EObject>toIterable(resource.getAllContents()), Entity.class);
      for(final Entity e : _filter_1) {
        _builder.append("class ");
        String _name = e.getName();
        _builder.append(_name);
        _builder.append("Factory(factory.Factory):");
        _builder.newLineIfNotEmpty();
        _builder.append("    ");
        _builder.append("class Meta:");
        _builder.newLine();
        _builder.append("        ");
        _builder.append("model = ");
        String _name_1 = e.getName();
        _builder.append(_name_1, "        ");
        _builder.newLineIfNotEmpty();
        _builder.newLine();
      }
    }
    return _builder;
  }
  
  private CharSequence createServices(final br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Module m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("from sqlalchemy import create_engine");
    _builder.newLine();
    _builder.append("from sqlalchemy.orm import sessionmaker");
    _builder.newLine();
    _builder.append("from ");
    _builder.append(this.lib_name);
    _builder.append(".model.");
    String _lowerCase = m.getName().toLowerCase();
    _builder.append(_lowerCase);
    _builder.append(".models import *");
    _builder.newLineIfNotEmpty();
    _builder.append("from ");
    _builder.append(this.lib_name);
    _builder.append(".service.base_service import BaseService");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    {
      EList<AbstractElement> _elements = m.getElements();
      for(final AbstractElement e : _elements) {
        {
          Class<?> _instanceClass = e.eClass().getInstanceClass();
          boolean _equals = Objects.equal(_instanceClass, Entity.class);
          if (_equals) {
            final Entity entity = ((Entity) e);
            _builder.newLineIfNotEmpty();
            CharSequence _service = this.service(entity);
            _builder.append(_service);
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    return _builder;
  }
  
  private CharSequence service(final Entity e) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("class ");
    String _firstUpper = StringExtensions.toFirstUpper(e.getName());
    _builder.append(_firstUpper);
    _builder.append("Service(BaseService):");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("def __init__(self):");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("super(");
    String _firstUpper_1 = StringExtensions.toFirstUpper(e.getName());
    _builder.append(_firstUpper_1, "\t\t");
    _builder.append("Service,self).__init__(");
    String _firstUpper_2 = StringExtensions.toFirstUpper(e.getName());
    _builder.append(_firstUpper_2, "\t\t");
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence createApplications(final br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Module m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("from ");
    _builder.append(this.lib_name);
    _builder.append(".service.");
    String _lowerCase = m.getName().toLowerCase();
    _builder.append(_lowerCase);
    _builder.append(".service import *");
    _builder.newLineIfNotEmpty();
    _builder.append("from ");
    _builder.append(this.lib_name);
    _builder.append(".application.abstract_application import AbstractApplication");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    {
      EList<AbstractElement> _elements = m.getElements();
      for(final AbstractElement e : _elements) {
        {
          Class<?> _instanceClass = e.eClass().getInstanceClass();
          boolean _equals = Objects.equal(_instanceClass, Entity.class);
          if (_equals) {
            final Entity entity = ((Entity) e);
            _builder.newLineIfNotEmpty();
            CharSequence _application = this.application(entity);
            _builder.append(_application);
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    return _builder;
  }
  
  private CharSequence application(final Entity e) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("class Application");
    String _firstUpper = StringExtensions.toFirstUpper(e.getName());
    _builder.append(_firstUpper);
    _builder.append("(AbstractApplication):");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("def __init__(self):");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("super().__init__(");
    String _firstUpper_1 = StringExtensions.toFirstUpper(e.getName());
    _builder.append(_firstUpper_1, "\t");
    _builder.append("Service())");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence createModels(final br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Module m) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("from ");
    _builder.append(this.lib_name);
    _builder.append(".config.base import Entity");
    _builder.newLineIfNotEmpty();
    _builder.append("from sqlalchemy import Column, Boolean ,ForeignKey, Integer, DateTime, String, Text");
    _builder.newLine();
    _builder.append("from sqlalchemy.orm import relationship");
    _builder.newLine();
    _builder.append("from ");
    _builder.append(this.lib_name);
    _builder.append(".model.relationship.models import *");
    _builder.newLineIfNotEmpty();
    {
      EList<AbstractElement> _elements = m.getElements();
      for(final AbstractElement e : _elements) {
        {
          Class<?> _instanceClass = e.eClass().getInstanceClass();
          boolean _equals = Objects.equal(_instanceClass, Entity.class);
          if (_equals) {
            final Entity entity = ((Entity) e);
            _builder.newLineIfNotEmpty();
            CharSequence _model = this.model(entity);
            _builder.append(_model);
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    return _builder;
  }
  
  private CharSequence model(final Entity e) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    _builder.append("class ");
    String _name = e.getName();
    _builder.append(_name);
    {
      Entity _superType = e.getSuperType();
      boolean _tripleNotEquals = (_superType != null);
      if (_tripleNotEquals) {
        _builder.append("(");
        String _name_1 = e.getSuperType().getName();
        _builder.append(_name_1);
        _builder.append(")");
      } else {
        _builder.append("(Entity)");
      }
    }
    _builder.append(":");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    {
      Description _description = e.getDescription();
      boolean _notEquals = (!Objects.equal(_description, null));
      if (_notEquals) {
        _builder.append("\"\"\"");
        String _textfield = e.getDescription().getTextfield();
        _builder.append(_textfield, "\t");
        _builder.append("\"\"\"");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("is_instance_of = \"");
    String _name_2 = e.getEntity_type().getName();
    _builder.append(_name_2, "\t");
    _builder.append("\"");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("__tablename__  = \"");
    String _lowerCase = e.getName().toLowerCase();
    _builder.append(_lowerCase, "\t");
    _builder.append("\"");
    _builder.newLineIfNotEmpty();
    {
      boolean _contains = this.superEntities.contains(e);
      if (_contains) {
        _builder.append("\ttype = Column(String(50))");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      Entity _superType_1 = e.getSuperType();
      boolean _tripleNotEquals_1 = (_superType_1 != null);
      if (_tripleNotEquals_1) {
        _builder.append("\tid = Column(Integer, ForeignKey(\'");
        String _lowerCase_1 = e.getSuperType().getName().toLowerCase();
        _builder.append(_lowerCase_1);
        _builder.append(".id\'), primary_key=True)");
      }
    }
    _builder.newLineIfNotEmpty();
    {
      EList<Attribute> _attributes = e.getAttributes();
      for(final Attribute a : _attributes) {
        CharSequence _compile = this.compile(a);
        _builder.append(_compile);
        _builder.newLineIfNotEmpty();
      }
    }
    {
      EList<Relation> _relations = e.getRelations();
      for(final Relation r : _relations) {
        {
          Class<?> _instanceClass = r.eClass().getInstanceClass();
          boolean _equals = Objects.equal(_instanceClass, OneToMany.class);
          if (_equals) {
            _builder.append("\t");
            String _name_3 = r.getName();
            _builder.append(_name_3);
            _builder.append(" = relationship(\"");
            String _name_4 = r.getType().getName();
            _builder.append(_name_4);
            _builder.append("\", back_populates=\"");
            String _lowerCase_2 = e.getName().toLowerCase();
            _builder.append(_lowerCase_2);
            _builder.append("\") ");
          }
        }
        _builder.newLineIfNotEmpty();
        {
          Class<?> _instanceClass_1 = r.eClass().getInstanceClass();
          boolean _equals_1 = Objects.equal(_instanceClass_1, ManyToMany.class);
          if (_equals_1) {
            _builder.append("\t");
            String _name_5 = r.getName();
            _builder.append(_name_5);
            _builder.append(" = relationship(\"");
            String _name_6 = r.getType().getName();
            _builder.append(_name_6);
            _builder.append("\", secondary=association_");
            String _lowerCase_3 = e.getName().toLowerCase();
            _builder.append(_lowerCase_3);
            _builder.append("_");
            String _lowerCase_4 = r.getType().getName().toLowerCase();
            _builder.append(_lowerCase_4);
            _builder.append("_table) ");
          }
        }
        _builder.newLineIfNotEmpty();
      }
    }
    {
      for(final RelationData rd : this.relationsData) {
        CharSequence _compile_1 = this.compile(rd, e);
        _builder.append(_compile_1);
        _builder.newLineIfNotEmpty();
      }
    }
    {
      boolean _contains_1 = this.superEntities.contains(e);
      if (_contains_1) {
        _builder.append("\t__mapper_args__ = {\'polymorphic_identity\':\'team\',\'polymorphic_on\':type}");
      }
    }
    _builder.newLineIfNotEmpty();
    {
      Entity _superType_2 = e.getSuperType();
      boolean _tripleNotEquals_2 = (_superType_2 != null);
      if (_tripleNotEquals_2) {
        _builder.append("\t__mapper_args__ = {\'polymorphic_identity\':\'");
        String _lowerCase_5 = e.getName().toLowerCase();
        _builder.append(_lowerCase_5);
        _builder.append("\',}");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence compile(final Attribute a) {
    StringConcatenation _builder = new StringConcatenation();
    String _name = a.getName();
    _builder.append(_name);
    _builder.append(" = Column(");
    String _type = a.getType();
    _builder.append(_type);
    _builder.append(")");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    return _builder;
  }
  
  private CharSequence compile(final RelationData rd, final Entity e) {
    StringConcatenation _builder = new StringConcatenation();
    {
      boolean _equals = rd.getTargetClass().equals(e.getName());
      if (_equals) {
        String _lowerCase = rd.getSourceClass().toLowerCase();
        _builder.append(_lowerCase);
        _builder.append("_id = Column(Integer, ForeignKey(\'");
        String _lowerCase_1 = rd.getSourceClass().toLowerCase();
        _builder.append(_lowerCase_1);
        _builder.append(".id\'))");
        _builder.newLineIfNotEmpty();
        String _lowerCase_2 = rd.getSourceClass().toLowerCase();
        _builder.append(_lowerCase_2);
        _builder.append(" = relationship(\"");
        String _sourceClass = rd.getSourceClass();
        _builder.append(_sourceClass);
        _builder.append("\", back_populates=\"");
        String _relationsthipname = rd.getRelationsthipname();
        _builder.append(_relationsthipname);
        _builder.append("\") ");
        _builder.newLineIfNotEmpty();
        _builder.append("\t\t");
        _builder.newLine();
      }
    }
    return _builder;
  }
}
