/**
 * generated by Xtext 2.21.0
 */
package br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Oim2LDBPackage#getAbstractElement()
 * @model
 * @generated
 */
public interface AbstractElement extends EObject
{
} // AbstractElement
