/*
 * generated by Xtext 2.21.0
 */
package br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.serializer;

import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.About;
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Application;
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Attribute;
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Author;
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Author_Email;
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Configuration;
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Description;
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Entity;
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Entity_Type;
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Function;
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Import;
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Lib;
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.ManyToMany;
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Oim2LDBPackage;
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.OneToMany;
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.OneToOne;
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Repository;
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Software;
import br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.services.Oim2LDBGrammarAccess;
import com.google.inject.Inject;
import java.util.Set;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.xtext.Action;
import org.eclipse.xtext.Parameter;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.serializer.ISerializationContext;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;

@SuppressWarnings("all")
public class Oim2LDBSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private Oim2LDBGrammarAccess grammarAccess;
	
	@Override
	public void sequence(ISerializationContext context, EObject semanticObject) {
		EPackage epackage = semanticObject.eClass().getEPackage();
		ParserRule rule = context.getParserRule();
		Action action = context.getAssignedAction();
		Set<Parameter> parameters = context.getEnabledBooleanParameters();
		if (epackage == Oim2LDBPackage.eINSTANCE)
			switch (semanticObject.eClass().getClassifierID()) {
			case Oim2LDBPackage.ABOUT:
				sequence_About(context, (About) semanticObject); 
				return; 
			case Oim2LDBPackage.APPLICATION:
				sequence_Application(context, (Application) semanticObject); 
				return; 
			case Oim2LDBPackage.ATTRIBUTE:
				sequence_Attribute(context, (Attribute) semanticObject); 
				return; 
			case Oim2LDBPackage.AUTHOR:
				sequence_Author(context, (Author) semanticObject); 
				return; 
			case Oim2LDBPackage.AUTHOR_EMAIL:
				sequence_Author_Email(context, (Author_Email) semanticObject); 
				return; 
			case Oim2LDBPackage.CONFIGURATION:
				sequence_Configuration(context, (Configuration) semanticObject); 
				return; 
			case Oim2LDBPackage.DESCRIPTION:
				sequence_Description(context, (Description) semanticObject); 
				return; 
			case Oim2LDBPackage.ENTITY:
				sequence_Entity(context, (Entity) semanticObject); 
				return; 
			case Oim2LDBPackage.ENTITY_TYPE:
				sequence_Entity_Type(context, (Entity_Type) semanticObject); 
				return; 
			case Oim2LDBPackage.FUNCTION:
				sequence_Function(context, (Function) semanticObject); 
				return; 
			case Oim2LDBPackage.IMPORT:
				sequence_Import(context, (Import) semanticObject); 
				return; 
			case Oim2LDBPackage.LIB:
				sequence_Lib(context, (Lib) semanticObject); 
				return; 
			case Oim2LDBPackage.MANY_TO_MANY:
				sequence_ManyToMany(context, (ManyToMany) semanticObject); 
				return; 
			case Oim2LDBPackage.MODULE:
				sequence_Module(context, (br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Module) semanticObject); 
				return; 
			case Oim2LDBPackage.ONE_TO_MANY:
				sequence_OneToMany(context, (OneToMany) semanticObject); 
				return; 
			case Oim2LDBPackage.ONE_TO_ONE:
				sequence_OneToOne(context, (OneToOne) semanticObject); 
				return; 
			case Oim2LDBPackage.REPOSITORY:
				sequence_Repository(context, (Repository) semanticObject); 
				return; 
			case Oim2LDBPackage.SOFTWARE:
				sequence_Software(context, (Software) semanticObject); 
				return; 
			}
		if (errorAcceptor != null)
			errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Contexts:
	 *     About returns About
	 *
	 * Constraint:
	 *     name=STRING
	 */
	protected void sequence_About(ISerializationContext context, About semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, Oim2LDBPackage.Literals.ABOUT__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, Oim2LDBPackage.Literals.ABOUT__NAME));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getAboutAccess().getNameSTRINGTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Application returns Application
	 *
	 * Constraint:
	 *     ((configuration=Configuration abstractElements+=AbstractElement+) | abstractElements+=AbstractElement+)?
	 */
	protected void sequence_Application(ISerializationContext context, Application semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Attribute returns Attribute
	 *
	 * Constraint:
	 *     (description=Description? name=ID type=ID)
	 */
	protected void sequence_Attribute(ISerializationContext context, Attribute semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Author returns Author
	 *
	 * Constraint:
	 *     name=STRING
	 */
	protected void sequence_Author(ISerializationContext context, Author semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, Oim2LDBPackage.Literals.AUTHOR__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, Oim2LDBPackage.Literals.AUTHOR__NAME));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getAuthorAccess().getNameSTRINGTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Author_Email returns Author_Email
	 *
	 * Constraint:
	 *     name=STRING
	 */
	protected void sequence_Author_Email(ISerializationContext context, Author_Email semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, Oim2LDBPackage.Literals.AUTHOR_EMAIL__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, Oim2LDBPackage.Literals.AUTHOR_EMAIL__NAME));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getAuthor_EmailAccess().getNameSTRINGTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Configuration returns Configuration
	 *
	 * Constraint:
	 *     (
	 *         software=Software 
	 *         about=About 
	 *         lib=Lib 
	 *         author=Author 
	 *         author_email=Author_Email 
	 *         repository=Repository
	 *     )
	 */
	protected void sequence_Configuration(ISerializationContext context, Configuration semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, Oim2LDBPackage.Literals.CONFIGURATION__SOFTWARE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, Oim2LDBPackage.Literals.CONFIGURATION__SOFTWARE));
			if (transientValues.isValueTransient(semanticObject, Oim2LDBPackage.Literals.CONFIGURATION__ABOUT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, Oim2LDBPackage.Literals.CONFIGURATION__ABOUT));
			if (transientValues.isValueTransient(semanticObject, Oim2LDBPackage.Literals.CONFIGURATION__LIB) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, Oim2LDBPackage.Literals.CONFIGURATION__LIB));
			if (transientValues.isValueTransient(semanticObject, Oim2LDBPackage.Literals.CONFIGURATION__AUTHOR) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, Oim2LDBPackage.Literals.CONFIGURATION__AUTHOR));
			if (transientValues.isValueTransient(semanticObject, Oim2LDBPackage.Literals.CONFIGURATION__AUTHOR_EMAIL) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, Oim2LDBPackage.Literals.CONFIGURATION__AUTHOR_EMAIL));
			if (transientValues.isValueTransient(semanticObject, Oim2LDBPackage.Literals.CONFIGURATION__REPOSITORY) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, Oim2LDBPackage.Literals.CONFIGURATION__REPOSITORY));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getConfigurationAccess().getSoftwareSoftwareParserRuleCall_2_0(), semanticObject.getSoftware());
		feeder.accept(grammarAccess.getConfigurationAccess().getAboutAboutParserRuleCall_3_0(), semanticObject.getAbout());
		feeder.accept(grammarAccess.getConfigurationAccess().getLibLibParserRuleCall_4_0(), semanticObject.getLib());
		feeder.accept(grammarAccess.getConfigurationAccess().getAuthorAuthorParserRuleCall_5_0(), semanticObject.getAuthor());
		feeder.accept(grammarAccess.getConfigurationAccess().getAuthor_emailAuthor_EmailParserRuleCall_6_0(), semanticObject.getAuthor_email());
		feeder.accept(grammarAccess.getConfigurationAccess().getRepositoryRepositoryParserRuleCall_7_0(), semanticObject.getRepository());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Description returns Description
	 *
	 * Constraint:
	 *     textfield=STRING
	 */
	protected void sequence_Description(ISerializationContext context, Description semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, Oim2LDBPackage.Literals.DESCRIPTION__TEXTFIELD) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, Oim2LDBPackage.Literals.DESCRIPTION__TEXTFIELD));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getDescriptionAccess().getTextfieldSTRINGTerminalRuleCall_1_0(), semanticObject.getTextfield());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     AbstractElement returns Entity
	 *     Entity returns Entity
	 *
	 * Constraint:
	 *     (
	 *         description=Description? 
	 *         name=ID 
	 *         superType=[Entity|QualifiedName]? 
	 *         entity_type=Entity_Type 
	 *         attributes+=Attribute* 
	 *         functions+=Function* 
	 *         relations+=Relation*
	 *     )
	 */
	protected void sequence_Entity(ISerializationContext context, Entity semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Entity_Type returns Entity_Type
	 *
	 * Constraint:
	 *     name=STRING
	 */
	protected void sequence_Entity_Type(ISerializationContext context, Entity_Type semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, Oim2LDBPackage.Literals.ENTITY_TYPE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, Oim2LDBPackage.Literals.ENTITY_TYPE__NAME));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getEntity_TypeAccess().getNameSTRINGTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Function returns Function
	 *
	 * Constraint:
	 *     (description=Description? name=ID (params+=ID params+=ID*)? type=ID)
	 */
	protected void sequence_Function(ISerializationContext context, Function semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     AbstractElement returns Import
	 *     Import returns Import
	 *
	 * Constraint:
	 *     importedNamespace=QualifiedNameWithWildcard
	 */
	protected void sequence_Import(ISerializationContext context, Import semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, Oim2LDBPackage.Literals.IMPORT__IMPORTED_NAMESPACE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, Oim2LDBPackage.Literals.IMPORT__IMPORTED_NAMESPACE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_0(), semanticObject.getImportedNamespace());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Lib returns Lib
	 *
	 * Constraint:
	 *     name=STRING
	 */
	protected void sequence_Lib(ISerializationContext context, Lib semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, Oim2LDBPackage.Literals.LIB__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, Oim2LDBPackage.Literals.LIB__NAME));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getLibAccess().getNameSTRINGTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Relation returns ManyToMany
	 *     ManyToMany returns ManyToMany
	 *
	 * Constraint:
	 *     (name=ID type=[Entity|ID])
	 */
	protected void sequence_ManyToMany(ISerializationContext context, ManyToMany semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, Oim2LDBPackage.Literals.RELATION__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, Oim2LDBPackage.Literals.RELATION__NAME));
			if (transientValues.isValueTransient(semanticObject, Oim2LDBPackage.Literals.RELATION__TYPE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, Oim2LDBPackage.Literals.RELATION__TYPE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getManyToManyAccess().getNameIDTerminalRuleCall_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getManyToManyAccess().getTypeEntityIDTerminalRuleCall_2_0_1(), semanticObject.eGet(Oim2LDBPackage.Literals.RELATION__TYPE, false));
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Module returns Module
	 *     AbstractElement returns Module
	 *
	 * Constraint:
	 *     (description=Description? name=QualifiedName elements+=AbstractElement*)
	 */
	protected void sequence_Module(ISerializationContext context, br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.oim2LDB.Module semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Contexts:
	 *     Relation returns OneToMany
	 *     OneToMany returns OneToMany
	 *
	 * Constraint:
	 *     (name=ID type=[Entity|ID])
	 */
	protected void sequence_OneToMany(ISerializationContext context, OneToMany semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, Oim2LDBPackage.Literals.RELATION__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, Oim2LDBPackage.Literals.RELATION__NAME));
			if (transientValues.isValueTransient(semanticObject, Oim2LDBPackage.Literals.RELATION__TYPE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, Oim2LDBPackage.Literals.RELATION__TYPE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getOneToManyAccess().getNameIDTerminalRuleCall_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getOneToManyAccess().getTypeEntityIDTerminalRuleCall_2_0_1(), semanticObject.eGet(Oim2LDBPackage.Literals.RELATION__TYPE, false));
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Relation returns OneToOne
	 *     OneToOne returns OneToOne
	 *
	 * Constraint:
	 *     (name=ID type=[Entity|ID])
	 */
	protected void sequence_OneToOne(ISerializationContext context, OneToOne semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, Oim2LDBPackage.Literals.RELATION__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, Oim2LDBPackage.Literals.RELATION__NAME));
			if (transientValues.isValueTransient(semanticObject, Oim2LDBPackage.Literals.RELATION__TYPE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, Oim2LDBPackage.Literals.RELATION__TYPE));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getOneToOneAccess().getNameIDTerminalRuleCall_0_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getOneToOneAccess().getTypeEntityIDTerminalRuleCall_2_0_1(), semanticObject.eGet(Oim2LDBPackage.Literals.RELATION__TYPE, false));
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Repository returns Repository
	 *
	 * Constraint:
	 *     name=STRING
	 */
	protected void sequence_Repository(ISerializationContext context, Repository semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, Oim2LDBPackage.Literals.REPOSITORY__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, Oim2LDBPackage.Literals.REPOSITORY__NAME));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getRepositoryAccess().getNameSTRINGTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Contexts:
	 *     Software returns Software
	 *
	 * Constraint:
	 *     name=STRING
	 */
	protected void sequence_Software(ISerializationContext context, Software semanticObject) {
		if (errorAcceptor != null) {
			if (transientValues.isValueTransient(semanticObject, Oim2LDBPackage.Literals.SOFTWARE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, Oim2LDBPackage.Literals.SOFTWARE__NAME));
		}
		SequenceFeeder feeder = createSequencerFeeder(context, semanticObject);
		feeder.accept(grammarAccess.getSoftwareAccess().getNameSTRINGTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
}
