/*
 * generated by Xtext 2.21.0
 */
package br.ufes.nemo.seon.generator.ontologyinformationmodel.to.database.lib.parser.antlr;

import java.io.InputStream;
import org.eclipse.xtext.parser.antlr.IAntlrTokenFileProvider;

public class Oim2LDBAntlrTokenFileProvider implements IAntlrTokenFileProvider {

	@Override
	public InputStream getAntlrTokenFile() {
		ClassLoader classLoader = getClass().getClassLoader();
		return classLoader.getResourceAsStream("br/ufes/nemo/seon/generator/ontologyinformationmodel/to/database/lib/parser/antlr/internal/InternalOim2LDB.tokens");
	}
}
